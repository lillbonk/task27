const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const session = require("express-session");
require("dotenv").config();
const app = express();

app.use(cookieParser());

app.use(
  session({
    secret: process.env.secret,
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      httpOnly: true,
      maxAge: 3600000,
    },
  })
);

app.get("/", (req, res) => {
  if (req.session.counter === undefined) {
    console.log("No session exists for this user...");
    console.log(req.session);
    
    res.redirect("/login");
  } else {
    console.log("Times you have visited this site:" + req.session.counter);
    req.session.counter += 1;
    res.redirect("/dashboard");
  }
});

app.get("/create", (req, res) => {
  console.log("Session created");
  req.session.counter = 0;
  console.log(req.session.counter);
  
  res.send("Session created");
});

app.get("/login", (req, res) => {
  res.send("This is the login Screen...");
});

app.get("/dashboard", (req, res) => {
  res.send("This is the Dashboard...");
});

app.listen(3000, () => console.log("Server started on port 3000..."));
